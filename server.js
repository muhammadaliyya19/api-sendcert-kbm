require('dotenv').config();
const host = process.env.HOST || 'http://localhost';
const port = process.env.PORT || 3000;
const host_swagger = process.env.URL_SWAGGER || 'http://localhost:3000'
const express = require('express');
var cors = require('cors');
const app = express();
const bodyParser = require('body-parser')
const morgan = require('morgan');
const path = require('path');
const fs = require('fs');
const swaggerDocs = require('swagger-ui-express');
const winston = require('./config/logger');
const swaggerConfig = require('./config/swagger');
// const schedule = require('node-schedule');
// const c_rec_dashboard = require('./app/controllers/setting_apps/c_rec_dashboard');
// const c_mail_outstd_dftr_asr = require('./app/controllers/setting_apps/c_mail_outstd_dftr_asr');
// const c_check_user_staff = require('./app/controllers/setting_apps/c_check_user_staff');
const log = require('./config/logger');

// For mongo
const mongoose = require("mongoose");
const mongoDB_con = require("./config/db_mongo").mongoURI;
// const bodyParser_mongo = require('body-parser')

mongoose.set('strictQuery', false);
mongoose.connect(mongoDB_con)
    .then(() => console.log("Koneksi mongo sukses"))
    .catch((err) => console.log(err));

// app.use(bodyParser_mongo.urlencoded({ extended: false, }));
// app.use(bodyParser_mongo.json());

// Default app config

app.use(cors());
app.use(bodyParser.urlencoded({ extended: true, }));
app.use(bodyParser.json());

var logDirectory = path.join(__dirname, "logs");
fs.existsSync(logDirectory) || fs.mkdirSync(logDirectory);

app.use(morgan(function (tokens, req, res) {
    return [
        tokens['remote-addr'](req, res),
        tokens.method(req, res),
        tokens.url(req, res),
        tokens.status(req, res),
        tokens.res(req, res, 'content-length'), '-',
        tokens['response-time'](req, res), 'ms',
        tokens['user-agent'](req, res)
    ].join(' ')
}, { stream: winston.stream }));

const routes = require('./app/routes/index');
for (var key in routes) {
    var modules = routes[key]
    app.use(modules)
}

// schedule.scheduleJob('0 6 * * *', function () {
//     log.info({ info: 'Running process record dashboard at 06:00' });
//     c_rec_dashboard.recordDashboard();
// });

// schedule.scheduleJob('0 7 * * *', function () {
//     log.info({ info: 'Running email outstanding asuransi at 07:00' });
//     c_mail_outstd_dftr_asr.sendEmail();
// });

// schedule.scheduleJob('0 18 * * *', function () {
//     log.info({ info: 'Running email outstanding asuransi at 18:00' });
//     c_mail_outstd_dftr_asr.sendEmail();
// });

// schedule.scheduleJob('0 0 * * *', function () {
//     log.info({ info: 'Running check last change password user staff at 00:00' });
//     c_check_user_staff.checkLastChangePassword();
// });

// schedule.scheduleJob('0 0 * * *', function () {
//     log.info({ info: 'Running check last login user staff at 00:00' });
//     c_check_user_staff.checkLastLogin();
// });
var options = {
    swaggerOptions: {
        url: `${host_swagger}/documentation/swagger.json`,
    },
    explorer: true
}

app.get("/documentation/swagger.json", (req, res) => res.json(swaggerConfig));
app.use('/documentation', swaggerDocs.serveFiles(null, options), swaggerDocs.setup(null, options));

app.use(express.static('assets'));
app.use('/assets', express.static('assets'));
app.use(function (req, res) {
    if (req.path == '/') {
        res.sendFile(path.join(__dirname + '/index.html'));
    } else {
        return res
            .status(404)
            .header('Content-Type', 'application/json; charset=utf-8')
            .send({
                code: 404,
                message: "Route " + req.method + ":" + req.url + " Not Found",
                error: "Not found",
            });
    }
});

app.listen(port, () => {
    console.log(`rest-api listening at ${host}:${port}`)
});