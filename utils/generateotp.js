"use-strict"
function generateOTP(charLen = 4) {
    charLen = charLen / 2
    var OTP = '';
    var Upper = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    var Number = '0123456789';
    for (let i = 1; i <= charLen; i++) {
        var p_Upper = Math.floor(Math.random()
            * Upper.length);
        var p_Number = Math.floor(Math.random()
            * Number.length);
        OTP += (Upper.substring(p_Upper, p_Upper + 1) + Number.substring(p_Number, p_Number + 1))
    }
    return OTP;
}

module.exports = {
    generateOTP
}