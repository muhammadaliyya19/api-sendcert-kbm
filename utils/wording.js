'user strict';

function generalListSuccess(name) {
    return "Berhasil mendapatkan daftar " + name;
}

function generalListError(name) {
    return "Gagal mendapatkan daftar " + name;
}

function generalDataSuccess(name) {
    return "Berhasil mendapatkan data " + name;
}

function generalDataError(name) {
    return "Gagal mendapatkan data " + name;
}

function generalSaveSuccess(name) {
    return "Berhasil menyimpan data " + name;
}

function generalSaveError(name) {
    return "Gagal menyimpan data " + name;
}

function generalUpdateSuccess(name) {
    return "Berhasil memperbarui data " + name;
}

function generalUpdateSuccessNodata(name) {
    return "Tidak ada data" + name + " yang diperbarui";
}

function generalUpdateError(name) {
    return "Gagal memperbarui data " + name;
}

function generalDeleteSuccess(name) {
    return "Berhasil menghapus data " + name;
}

function generalDeleteSuccessNodata(name) {
    return "Tidak ada data" + name + " yang dihapus";
}

function generalDeleteError(name) {
    return "Gagal menghapus data " + name;
}

function generalActivateSuccess(name) {
    return "Berhasil mengaktifkan data " + name;
}

function generalActivateError(name) {
    return "Gagal mengaktifkan data " + name;
}

function generalDeactivateSuccess(name) {
    return "Berhasil menonaktifkan data " + name;
}

function generalDeactivateError(name) {
    return "Gagal menonaktifkan data " + name;
}

function generalCancelSuccess(name) {
    return "Berhasil membatalkan data " + name;
}

function generalCancelError(name) {
    return "Gagal membatalkan data " + name;
}

function generalVoidSuccess(name) {
    return "Berhasil void data " + name;
}

function generalVoidSuccessNodata(name) {
    return "Tidak ada data" + name + " yang divoid";
}

function generalVoidError(name) {
    return "Gagal melakukan void data " + name;
}

function generalApproveSuccess(name) {
    return "Berhasil melakukan menyetujui data " + name;
}

function generalApproveError(name) {
    return "Gagal melakukan menyetujui data " + name;
}

function generalSendBackSuccess(name) {
    return "Berhasil merevisi data " + name;
}

function generalSendBackError(name) {
    return "Gagal merevisi data " + name;
}

function generalGetIDCardError(name) {
    return name + " tidak sesuai dengan user anda, periksa kembali inputan " + name;
}



module.exports = {
    generalListSuccess,
    generalListError,
    generalDataSuccess,
    generalDataError,
    generalSaveSuccess,
    generalSaveError,
    generalUpdateSuccess,
    generalUpdateSuccessNodata,
    generalUpdateError,
    generalDeleteSuccess,
    generalDeleteError,
    generalDeleteSuccessNodata,
    generalActivateSuccess,
    generalActivateError,
    generalDeactivateSuccess,
    generalDeactivateError,
    generalCancelSuccess,
    generalCancelError,
    generalVoidSuccess,
    generalVoidSuccessNodata,
    generalVoidError,
    generalApproveSuccess,
    generalApproveError,
    generalSendBackSuccess,
    generalSendBackError,
    generalGetIDCardError
}