"use-strict"
function generatePassword() {
    var newPassword = '';
    var Upper = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    var Lower = 'abcdefghijklmnopqrstuvwxyz';
    var Special = '!@#%&';
    var Number = '0123456789';
    for (let i = 1; i <= 2; i++) {
        var p_Upper = Math.floor(Math.random()
            * Upper.length);
        var p_Lower = Math.floor(Math.random()
            * Lower.length);
        var p_Special = Math.floor(Math.random()
            * Special.length);
        var p_Number = Math.floor(Math.random()
            * Number.length);
        newPassword += (Upper.substring(p_Upper, p_Upper + 1) + Special.substring(p_Special, p_Special + 1) + Number.substring(p_Number, p_Number + 1) + Lower.substring(p_Lower, p_Lower + 1))
    }
    return newPassword;
}

module.exports = {
    generatePassword
}