'user strict'

var hari = ['Minggu', 'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jum&#39;at', 'Sabtu'];
var bulan = ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'];

function dateTimeGenerator() {
    let d = new Date(),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear(),
        hour = d.getHours(),
        minute = d.getMinutes(),
        second = d.getSeconds();

    if (month.length < 2)
        month = '0' + month;
    if (day.length < 2)
        day = '0' + day;
    var dt = [year, month, day].join('-');
    var tm = [hour, minute, second].join(':');
    return dt + " " + tm;
}

function dateGenerator() {
    let d = new Date(),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2)
        month = '0' + month;
    if (day.length < 2)
        day = '0' + day;
    var dt = [year, month, day].join('-');
    return dt;
}

function dateFileGenerator() {
    let d = new Date(),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2)
        month = '0' + month;
    if (day.length < 2)
        day = '0' + day;
    var dt = [year, month, day].join('');
    return dt;
}

function dayIndonesiaGenerator(interval = 0) {
    let date;
    if (interval == 0) {
        date = new Date();
    } else {
        date = new Date(new Date().getTime());
        date.setDate(date.getDate() + interval);
    }
    var thisDay = hari[date.getDay()];

    return thisDay;
}

function dateIndonesiaGenerator(interval = 0) {
    let d = new Date();
    if (interval == 0) {
        d = new Date();
    } else {
        d = new Date(new Date().getTime());
        d.setDate(d.getDate() + interval);
    }
    let month = '' + d.getMonth(),
        day = '' + d.getDate(),
        year = d.getFullYear();



    let dt = day + ' ' + bulan[month] + ' ' + year;

    return dt;
}

function dConvert(dt) {
    let d = new Date(dt),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear(),
        hour = d.getHours(),
        minute = d.getMinutes(),
        second = d.getSeconds();

    if (month.length < 2)
        month = '0' + month;
    if (day.length < 2)
        day = '0' + day;
    var dt = [year, month, day].join('-');
    var tm = [hour, minute, second].join(':');
    return dt + " " + tm;
}

function diffMinutes(dt, minute) {
    let d = new Date(new Date(dt).getTime() + (minute * 60000));
    if (d < new Date()) {
        return true;
    } else {
        return false;
    }
}

function dateGeneratorFromEpoch(epoch) {
    let d = new Date(epoch),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2)
        month = '0' + month;
    if (day.length < 2)
        day = '0' + day;
    var dt = [year, month, day].join('-');
    return dt;
}
function timeGeneratorFromEpoch(epoch) {
    let d = new Date(epoch),
        hour = d.getHours(),
        minute = d.getMinutes(),
        second = d.getSeconds();

    var tm = [hour, minute, second].join(':');
    return tm;
}
function previousDate(date = new Date()) {
    const previous = new Date(date.getTime());
    previous.setDate(date.getDate() - 1);
    let month = '' + (previous.getMonth() + 1),
        day = '' + previous.getDate(),
        year = previous.getFullYear();

    var dt = [year, month, day].join('-');
    return dt;
}


module.exports = {
    dateTimeGenerator,
    dateGenerator,
    dConvert,
    diffMinutes,
    dateFileGenerator,
    dayIndonesiaGenerator,
    dateIndonesiaGenerator,
    dateGeneratorFromEpoch,
    timeGeneratorFromEpoch,
    previousDate
}