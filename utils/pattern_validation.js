"use-strict"

let msg_length, msg_upper, msg_lower, msg_number, msg_special, arrMsg;
const upper = new RegExp(
    "^(?=.*[A-Z]).+$"),
    lower = new RegExp(
        "^(?=.*[a-z]).+$"),
    number = new RegExp(
        "^(?=.*\\d).+$"),
    special = new RegExp(
        "^(?=.*[-+_!@#$%^&*.,?]).+$");

function checkPattern(password) {
    let msg = [];
    if (!password || password.length < 8) {
        msg_length = "Minimal 8 karakter";
    } else {
        msg_length = null
    }
    if (upper.test(password)) {
        msg_upper = null;
    } else {
        msg_upper = "Minimal 1 huruf kapital";
    }
    if (lower.test(password)) {
        msg_lower = null;
    } else {
        msg_lower = "Minimal 1 huruf kecil";
    }
    if (number.test(password)) {
        msg_number = null;
    } else {
        msg_number = "Minimal 1 nomor";
    }
    if (special.test(password)) {
        msg_special = null;
    } else {
        msg_special = "Minimal 1 karakter spesial. Misalnya: - + _ ! @ # $ % & * . , ?";
    }

    msg.push(msg_length, msg_upper, msg_lower, msg_number, msg_special)

    arrMsg = msg.filter(function (el) {
        return el != null;
    });

    if (arrMsg.length > 0) {
        return arrMsg
    } else {
        return true
    }
}

module.exports = {
    checkPattern
}