require('dotenv').config();

const nodemailer = require("nodemailer");
const { google } = require("googleapis");
const OAuth2 = google.auth.OAuth2;
const Mailgen = require("mailgen");
const log = require('../config/logger');


async function createTransporter(request, reply) {
    try {
        const oauth2Client = new OAuth2(
            process.env.CLIENT_ID,
            process.env.CLIENT_SECRET,
            "https://developers.google.com/oauthplayground"
        );

        oauth2Client.setCredentials({
            refresh_token: process.env.REFRESH_TOKEN
        });

        const accessToken = await new Promise((resolve, reject) => {
            oauth2Client.getAccessToken((err, token) => {
                if (err) {
                    log.error({ error: err, msg: err.message });
                    reject("Failed to create access token :(");
                }
                resolve(token);
            });
        });

        const transporter = nodemailer.createTransport({
            service: "gmail",
            auth: {
                type: "OAuth2",
                user: process.env.EMAIL,
                accessToken,
                clientId: process.env.CLIENT_ID,
                clientSecret: process.env.CLIENT_SECRET,
                refreshToken: process.env.REFRESH_TOKEN
            }
        });

        return transporter;
    } catch (error) {
        log.error({ error: error, msg: error.message });
        return error;
    }
}

async function sendEmail(id, emailaddr, token, minute) {
    try {
        let emailTransporter = await createTransporter();
        var mailGenerator = new Mailgen({
            theme: "default",
            product: {
                name: "IT PT. KBM",
                link: process.env.WEB_URL,
            },
        });

        var email = {
            body: {
                greeting: false,
                intro: 'Halo ' + id,
                action: {
                    instructions: 'Masukkan kode berikut untuk melakukan perubahan kata sandi.',
                    button: {
                        color: '#22BC66', // Optional action button color
                        text: token,
                    }
                },
                outro: 'Kode di atas hanya berlaku '+ minute +' menit. Mohon jangan sebarkan kode ini ke siapapun, termasuk pihak yang mengatasnamakan KBM. Butuh bantuan atau ada yang ditanyakan? Kontak ke bagian IT, kami senang untuk membantu.',
                signature: 'Salam hormat'
            }
        };

        var emailBody = mailGenerator.generate(email);

        let mailOptions = {
            from: process.env.EMAIL,
            to: emailaddr, // list of receivers
            subject: "Kode Verifikasi perubahan password", // Subject line
            html: emailBody
        };
        await emailTransporter.sendMail(mailOptions);

        log.info({ email: emailaddr, msg: "Success Sending OTP" });
        return true;
    } catch (error) {
        log.error({ error: error, msg: error.message });
        return false;
    }

}

async function sendEmailAttachment(id, filepath, emailaddr, nama_peserta) {
    try {
        let emailTransporter = await createTransporter();
        var mailGenerator = new Mailgen({
            theme: "default",
            product: {
                name: "IT PT. KBM",
                link: process.env.WEB_URL,
            },
        });

        var email = {
            body: {
                greeting: false,
                intro: 'Halo ' + id,
                action: {
                    instructions: 'Silakan Download Sertifikat Pelatihan Anda',
                    button: {
                        color: '#22BC66', // Optional action button color
                        text: 'token',
                    }
                },
                outro: 'Terima kasih telah hadir dan turut menyukseskan program ini.',
                signature: 'Salam hormat'
            }
        };

        var emailBody = mailGenerator.generate(email);

        let mailOptions = {
            from: process.env.EMAIL,
            to: emailaddr, // list of receivers
            subject: "Sertifikat Pelatihan", // Subject line
            html: emailBody,
            attachments: [
                {
                    filename: nama_peserta+'.pdf',
                    path: filepath,
                }
            ]
        };
        await emailTransporter.sendMail(mailOptions);

        log.info({ email: emailaddr, msg: "Success Sending OTP" });
        return true;
    } catch (error) {
        log.error({ error: error, msg: error.message });
        return false;
    }

}

async function sendEmailPassword(id, emailaddr, pwd) {
    try {
        let emailTransporter = await createTransporter();
        var mailGenerator = new Mailgen({
            theme: "default",
            product: {
                name: "IT PT. KBM",
                link: process.env.WEB_URL,
            },
        });

        var email = {
            body: {
                greeting: false,
                intro: 'Halo ' + id,
                action: {
                    instructions: 'Silahkan kembali ke apilikasi untuk login dengan password baru anda.',
                    button: {
                        color: '#2a7cf6', // Optional action button color
                        text: pwd,
                        textStyle: 'bold'
                    }
                },
                outro: 'Mohon jangan sebarkan password anda ke siapapun, termasuk pihak yang mengatasnamakan KBM. Butuh bantuan atau ada yang ditanyakan? Kontak ke bagian IT, kami senang untuk membantu.',
                signature: 'Salam hormat'
            }
        };

        var emailBody = mailGenerator.generate(email);

        let mailOptions = {
            from: process.env.EMAIL,
            to: emailaddr, // list of receivers
            subject: "Password Baru User Staff", // Subject line
            html: emailBody
        };

        await emailTransporter.sendMail(mailOptions);
        log.info({ email: emailaddr, msg: "Success Sending Password" });
        return true;
    } catch (error) {
        log.error({ error: error, msg: error.message });
        return false;
    }

}

module.exports = {
    sendEmail,
    sendEmailPassword,
    sendEmailAttachment
}
