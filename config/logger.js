const winston = require('winston');
const DailyRotateFile = require('winston-daily-rotate-file');
const { createLogger, format, transports } = require('winston');
const { combine, splat, timestamp, printf, prettyPrint } = format;

const myFormat = printf( ({ level, message, timestamp}) => {
  return `${timestamp} [${level}] : ${JSON.stringify(message)}`
});

const transport = new DailyRotateFile({ 
    filename: 'logs/log-%DATE%.log', 
    datePattern: 'YYYY-MM-DD-HH', 
    zippedArchive: false, 
    maxSize: '100m', 
    maxFiles: '7d', 
    prepend: true,
    level: 'debug',
});

transport.on('rotate', function (oldFilename, newFilename) {

});

var logger = new winston.createLogger({
    format: combine(
        // format.colorize(),
        splat(),
        timestamp({ format: 'YYYY-MM-DD HH:mm:ss' }),
        myFormat
    ),
    transports: [
        transport
    ],
    exitOnError: false
});

module.exports = logger;

module.exports.stream = {
    write: function(message, req){
        logger.info(message);
    }
};