require('dotenv').config()

module.exports = {
    'secret' : process.env.JWT_SECRET,
    'expiresIn' : process.env.JWT_EXPRIED,
    'ftp_host' : process.env.FTP_HOST,
    'ftp_user' : process.env.FTP_USER,
    'ftp_pswd' : process.env.FTP_PASSWORD,
}