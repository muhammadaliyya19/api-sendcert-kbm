require('dotenv').config();

var knex = require('knex')({
    client: 'mysql',
    connection: {
        host: process.env.DB_HOST_KARY,
        user: process.env.DB_USERNAME_KARY,
        password: process.env.DB_PASSWORD_KARY,
        database: process.env.DB_DATABASE_KARY,
        timezone: 'Asia/Jakarta',
    },
    pool: { 
        min: 0, 
        max: 10,
    }, //Menggunakan fungsi pool agar menjaga koneksi ke DB tetep tersambung);
});

module.exports = knex;