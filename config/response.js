function ok (values, message, res) {
    return res
        .status(200)
        .header('Content-Type', 'application/json; charset=utf-8')
        .send({
            code : 200,
            error : false,
            message : message,
            values : values,
        })
}


function err(status, message, error_message, res) {
    return res
        .status(status)
        .header('Content-Type', 'application/json; charset=utf-8')
        .send({
            code: status,
            message: message,
            error: error_message,
        })
}


module.exports = { ok, err }