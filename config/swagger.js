require('dotenv').config();
const s_index = require("../app/schemas/s_index");

const swagger = {
    openapi : "3.0.0",
    info    : {
        title       : "Documentation of Service Training Certificate Corenity MPS - KBM",
        version     : "1.0",
        description : "Dokumentasi untuk Service Training Certificate / Sertifikat Pelatihan Corenity MPS KBM, anda dapat menggunakan dokumentasi ini untuk melakukan generate sertifikat pelatihan dan mengirimkannya ke peserta."
    },
    components  : {
        securitySchemes : {
            XVAKey: {
                description	: "Masukkan token user untuk mengakses endpoint\"",
                name	    : "Authorization",
                in	        : "header",
                type	    : "http",
                scheme	    : "bearer",
                bearerFormat: "JWT"
            }
        }
    },
    servers: [{
        url         : process.env.URL_SWAGGER,
        description : "local development"
    }],
    tags: [
        { name: 'generate-send-cert', description: 'Operasi untuk membuat dan mengirim sertifikat' },
        // { name: 'auth', description: 'Operasi untuk authentikasi pengguna' },
        // { name: 'setting-application', description: 'Operasi untuk setting aplikasi' },
        // { name: 'mobile-application', description: 'Operasi untuk pengguna mobile' },
        // { name: 'dashboard', description: 'Operasi untuk dashboard' },
        // { name: 'user-management', description: 'Operasi untuk manajemen user dan hak akses' },
        // { name: 'master', description: 'Operasi untuk data master' },
        // { name: 'recruitment', description: 'Operasi untuk modul rekruitmen' },
        // { name: 'korlap', description: 'Operasi untuk modul korlap' },
        // { name: 'asuransi', description: 'Operasi untuk modul asuransi' },
        // { name: 'marketing', description: 'Operasi untuk modul marketing' },
        // { name: 'accounting', description: 'Operasi untuk modul accounting' },
        // { name: 'report', description: 'Operasi untuk modul report' },
        // { name: 'tools', description: 'Operasi untuk modul tools' },
    ],
    paths: s_index,

}

module.exports = swagger;