const generateCert = {
    tags: ["generate-send-cert"],
    description: "endpoint ini berfungsi untuk men-Generate sertifikat dan mengirimkannya",
    responses: {
        200 : {
            description: "OK",
        }
    },
    // security: [
    //     {
    //         "XVAKey": []
    //     }
    // ],
    // parameters: [{
    //     name: "nik",
    //     in: "query",
    //     required: "true",
    //     schema: {
    //         type: "string"
    //     }
    // }]
}

const sendCert = {
    tags: ["generate-send-cert"],
    description: "endpoint ini berfungsi untuk mengirim sertifikat",
    responses: {
        200 : {
            description: "OK",
        }
    },
    // security: [
    //     {
    //         "XVAKey": []
    //     }
    // ],
}

const toolsDocs = {
    "/cert/generate-cert" : {
        get : generateCert,
    },
    "/cert/send-cert" : {
        get : sendCert,
    },
}

module.exports = toolsDocs;