const s_generate_cert = require('./cert/s_generate_cert');
const s_agama = require('./master/s_agama');
const s_agama_mongo = require('./master/s_agama_mongo');

const s_index = {
    ...s_generate_cert,
    ...s_agama,
    ...s_agama_mongo
}

module.exports = s_index  