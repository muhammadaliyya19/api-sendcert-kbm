const lsitAgama = {
    tags: ["master"],
    description: "Endpoint ini berfungsi untuk mendapatkan daftar semua agama.",
    responses: {
        200 : {
            description: "OK",
        }
    },
    // security: [
    //     {
    //         "XVAKey": []
    //     }
    // ]
}

const agamaByNama = {
    tags: ["master"],
    description: "Endpoint ini berfungsi untuk mendapatkan data agama berdasarkan nama agama.",
    responses: {
        200 : {
            description: "OK",
        }
    },
    // security: [
    //     {
    //         "XVAKey": []
    //     }
    // ],
    parameters: [{
        name: "agm_nama",
        in: "query",
        required: "true",
        schema: {
            type: "string"
        }
    }]
}

const saveAgama = {
    tags: ["master"],
    description: "Endpoint ini berfungsi untuk meyimpan data agama baru.",
    responses: {
        200 : {
            description: "OK",
        }
    },
    // security: [
    //     {
    //         "XVAKey": []
    //     }
    // ],
    requestBody: {
        content: {
            "application/json": {
                schema: {
                    type: "object",
                    properties: {
                        "values" : {
                            type : "object",
                            properties: {
                                "agm_nama" : {
                                    type : "string"
                                },
                            },
                        },
                    },
                    example: {
                        "values" : {
                            "agm_nama": ""
                        }
                    }
                }
            }
        }
    }
}

const updateAgama = {
    tags: ["master"],
    description: "Endpoint ini berfungsi untuk memperbarui data agama berdasarkan nama agama.",
    responses: {
        200 : {
            description: "OK",
        }
    },
    // security: [
    //     {
    //         "XVAKey": []
    //     }
    // ],
    requestBody: {
        content: {
            "application/json": {
                schema: {
                    type: "object",
                    properties: {
                        "conditions" : {
                            type : "object",
                            properties: {
                                "agm_nama_old" : {
                                    type : "string",
                                },
                            },
                        },
                        "values" : {
                            type : "object",
                            properties: {
                                "agm_nama" : {
                                    type : "string",

                                },
                            },
                        },
                    },
                    example: {
                        "conditions" : {
                            "agm_nama": "",
                        },
                        "values" : {
                            "agm_nama": "",
                        }
                    }
                }
            }
        }
    }
}

const deleteAgama = {
    tags: ["master"],
    description: "Endpoint ini berfungsi untuk menghapus data agama berdasarkan nama agama.",
    responses: {
        200 : {
            description: "OK",
        }
    },
    // security: [
    //     {
    //         "XVAKey": []
    //     }
    // ],
    requestBody: {
        content: {
            "application/json": {
                schema: {
                    type: "object",
                    properties: {
                        "agm_nama" : {
                            type : "string"
                        },
                    },
                    example: {
                        "agm_nama": "",
                    }
                }
            }
        }
    }
}

const agamaDocs = {
    "/master/agama/list" : {
        get : lsitAgama,
    },
    "/master/agama/by-nama" : {
        get : agamaByNama,
    },
    "/master/agama/save" : {
        post : saveAgama,
    },
    "/master/agama/update" : {
        put : updateAgama,
    },
    "/master/agama/destroy" : {
        delete : deleteAgama,
    },
}

module.exports = agamaDocs;