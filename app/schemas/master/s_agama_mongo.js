const listAgama = {
    tags: ["master_mongo"],
    description: "Endpoint ini berfungsi untuk mendapatkan daftar semua agama.",
    responses: {
        200 : {
            description: "OK",
        }
    },
    // security: [
    //     {
    //         "XVAKey": []
    //     }
    // ]
}

const agamaByNama = {
    tags: ["master_mongo"],
    description: "Endpoint ini berfungsi untuk mendapatkan data agama berdasarkan nama agama.",
    responses: {
        200 : {
            description: "OK",
        }
    },
    // security: [
    //     {
    //         "XVAKey": []
    //     }
    // ],
    parameters: [{
        name: "nama",
        in: "query",
        required: "true",
        schema: {
            type: "string"
        }
    }]
}

const saveAgama = {
    tags: ["master_mongo"],
    description: "Endpoint ini berfungsi untuk meyimpan data agama baru.",
    responses: {
        200 : {
            description: "OK",
        }
    },
    // security: [
    //     {
    //         "XVAKey": []
    //     }
    // ],
    requestBody: {
        content: {
            "application/json": {
                schema: {
                    type: "object",
                    properties: {
                        "_id" : {
                            type : "string"
                        },
                        "agm_nama" : {
                            type : "string"
                        },
                    },
                    example: {
                        "_id": "",
                        "agm_nama": "",
                    }
                }
            }
        }
    }
}

const updateAgama = {
    tags: ["master_mongo"],
    description: "Endpoint ini berfungsi untuk memperbarui data agama berdasarkan nama agama.",
    responses: {
        200 : {
            description: "OK",
        }
    },
    // security: [
    //     {
    //         "XVAKey": []
    //     }
    // ],
    requestBody: {
        content: {
            "application/json": {
                schema: {
                    type: "object",
                    properties: {
                        "conditions" : {
                            type : "object",
                            properties: {
                                "nama_old" : {
                                    type : "string",
                                },
                            },
                        },
                        "values" : {
                            type : "object",
                            properties: {
                                "nama" : {
                                    type : "string",

                                },
                            },
                        },
                    },
                    example: {
                        "conditions" : {
                            "nama_old": "",
                        },
                        "values" : {
                            "nama": "",
                        }
                    }
                }
            }
        }
    }
}

const deleteAgama = {
    tags: ["master_mongo"],
    description: "Endpoint ini berfungsi untuk menghapus data agama berdasarkan id agama.",
    responses: {
        200 : {
            description: "OK",
        }
    },
    // security: [
    //     {
    //         "XVAKey": []
    //     }
    // ],
    requestBody: {
        content: {
            "application/json": {
                schema: {
                    type: "object",
                    properties: {
                        "id" : {
                            type : "string"
                        },
                    },
                    example: {
                        "id": "",
                    }
                }
            }
        }
    }
}

const agamaDocs = {
    "/master_mongo/agama/list" : {
        get : listAgama,
    },
    "/master_mongo/agama/by-nama" : {
        get : agamaByNama,
    },
    "/master_mongo/agama/save" : {
        post : saveAgama,
    },
    "/master_mongo/agama/update" : {
        put : updateAgama,
    },
    "/master_mongo/agama/destroy" : {
        delete : deleteAgama,
    },
}

module.exports = agamaDocs;