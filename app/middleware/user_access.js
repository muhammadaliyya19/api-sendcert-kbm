'use strict';
const jwt = require('jsonwebtoken');
const response = require('../../config/response');
const config = require('../../config/app');
const log = require('../../config/logger');
const m_userrights = require('../models/user_management/m_userrights')

function readData(menuid) {
    return async (req, res, next) => {
        let token = req.headers.authorization;
        if (token) {
            token = token.replace('Bearer ', '');
            jwt.verify(token, config.secret, async function(error, decoded) {
                if (error) {
                    log.error( { body: req.body, msg: error} );
                    response.err(401, "Token tidak valid", "", res);
                } else {
                    const decodedToken = jwt.decode(token, { complete: false });
                    log.info({ endpoint: req.originalUrl, userid: decodedToken.id, query: req.query });
                    const userrights = await m_userrights
                                    .query()
                                    .findOne({
                                        menuid:menuid, 
                                        usergroupid: decodedToken.usergroupid
                                    });
                    if (userrights) {
                        if (userrights.read_rec) {
                            next();
                        } else {
                            log.info({body: req.body, msg: "User tidak memiliki akses"});
                            response.err(403, "User tidak memiliki akses", "", res);
                        }
                    } else {
                        log.info({body: req.body, msg: "User tidak memiliki akses"});
                        response.err(403, "User tidak memiliki akses", "", res);
                    }
                }
            });
        } else {
            log.info({body: req.body, msg: "User tidak diizinkan untuk mengakses endpoint"});
            response.err(401, "Tidak diizinkan untuk mengakses endpoint", "", res);
        }
    }
}

function createData(menuid) {
    return async (req, res, next) => {
        let token = req.headers.authorization;
        if (token) {
            token = token.replace('Bearer ', '');
            jwt.verify(token, config.secret, async function(error, decoded) {
                if (error) {
                    log.error( { body: req.body, msg: error} );
                    response.err(401, "Token tidak valid", "", res);
                } else {
                    const decodedToken = jwt.decode(token, { complete: false });
                    log.info({ endpoint: req.originalUrl, userid: decodedToken.id, body: req.body });
                    const userrights = await m_userrights
                                    .query()
                                    .findOne({
                                        menuid:menuid, 
                                        usergroupid: decodedToken.usergroupid
                                    });
                    
                    if (userrights) {
                        if (userrights.create_rec) {
                            next();
                        } else {
                            log.info({body: req.body, msg: "User tidak memiliki akses"});
                            response.err(403, "User tidak memiliki akses", "", res);
                        }
                    } else {
                        log.info({body: req.body, msg: "User tidak memiliki akses"});
                        response.err(403, "User tidak memiliki akses", "", res);
                    }
                }
            });
        } else {
            log.info({body: req.body, msg: "User tidak diizinkan untuk mengakses endpoint"});
            response.err(401, "Tidak diizinkan untuk mengakses endpoint", "", res);
        }
    }
}

function updateData(menuid) {
    return async (req, res, next) => {
        let token = req.headers.authorization;
        if (token) {
            token = token.replace('Bearer ', '');
            jwt.verify(token, config.secret, async function(error, decoded) {
                if (error) {
                    log.error( { body: req.body, msg: error} );
                    response.err(401, "Token tidak valid", "", res);
                } else {
                    const decodedToken = jwt.decode(token, { complete: false });
                    log.info({ endpoint: req.originalUrl, userid: decodedToken.id, body: req.body });
                    const userrights = await m_userrights
                                    .query()
                                    .findOne({
                                        menuid:menuid, 
                                        usergroupid: decodedToken.usergroupid
                                    });

                    if (userrights) {
                        if (userrights.update_rec) {
                            next();
                        } else {
                            log.info({body: req.body, msg: "User tidak memiliki akses"});
                            response.err(403, "User tidak memiliki akses", "", res);
                        }
                    } else {
                        log.info({body: req.body, msg: "User tidak memiliki akses"});
                        response.err(403, "User tidak memiliki akses", "", res);
                    }
                }
            });
        } else {
            log.info({body: req.body, msg: "User tidak diizinkan untuk mengakses endpoint"});
            response.err(401, "Tidak diizinkan untuk mengakses endpoint", "", res);
        }
    }
}

function deleteData(menuid) {
    return async (req, res, next) => {
        let token = req.headers.authorization;
        if (token) {
            token = token.replace('Bearer ', '');
            jwt.verify(token, config.secret, async function(error, decoded) {
                if (error) {
                    log.error( { body: req.body, msg: error} );
                    response.err(401, "Token tidak valid", "", res);
                } else {
                    const decodedToken = jwt.decode(token, { complete: false });
                    log.info({ endpoint: req.originalUrl, userid: decodedToken.id, body: req.body });
                    const userrights = await m_userrights
                                    .query()
                                    .findOne({
                                        menuid:menuid, 
                                        usergroupid: decodedToken.usergroupid
                                    });
                    
                    if (userrights) {
                        if (userrights.delete_rec) {
                            next();
                        } else {
                            log.info({body: req.body, msg: "User tidak memiliki akses"});
                            response.err(403, "User tidak memiliki akses", "", res);
                        }
                    } else {
                        log.info({body: req.body, msg: "User tidak memiliki akses"});
                        response.err(403, "User tidak memiliki akses", "", res);
                    }
                }
            });
        } else {
            log.info({body: req.body, msg: "User tidak diizinkan untuk mengakses endpoint"});
            response.err(401, "Tidak diizinkan untuk mengakses endpoint", "", res);
        }
    }
}

module.exports = {
    readData,
    createData,
    updateData,
    deleteData
}