'use strict';
const jwt = require('jsonwebtoken');
const response = require('../../config/response');
const log = require('../../config/logger');
const config = require('../../config/app');
const m_user = require('../models/user_management/m_user');

async function verifyToken(req, res, next) {
    let token = req.headers.authorization;
    if (token) {
        token = token.replace('Bearer ', '');
        jwt.verify(token, config.secret, async function (error, decoded) {
            if (error) {
                log.error({ body: req.body, msg: error });
                response.err(401, "Token tidak valid", "", res);
            } else {
                const decodedToken = jwt.decode(token, { complete: false });
                const data = await m_user
                    .query().findById(decodedToken.id);

                if (decodedToken.is_login !== data.is_login) {
                    log.info({ body: req.body, msg: "Anda telah login di perangkat " + data.last_login_device + " (" + data.last_login_at + ")" + " apabila aktivitas tersebut bukan anda, segera hubungi IT!" });
                    return response.err(401, "Anda telah login di perangkat " + data.last_login_device + " (" + data.last_login_at + ")" + " apabila aktivitas tersebut bukan anda, segera hubungi IT!", true, res);
                } else {
                    req.userid = decodedToken.id;
                    req.usergroupid = decodedToken.usergroupid;
                    req.userlevel = decodedToken.userlevel;
                    req.area_kode = decodedToken.area_kode;
                    req.user_type = decodedToken.user_type;
                    req.is_login = decodedToken.is_login;
                    next();
                }
            }
        });
    } else {
        log.info({ body: req.body, msg: "User tidak diizinkan untuk mengakses endpoint ini" });
        response.err(401, "User tidak diizinkan untuk mengakses endpoint ini", "", res);
    }
}

module.exports = {
    verifyToken
}