const { Model } = require('objection');
const knex = require('../../../config/database');
const formater = require('../../../utils/formater');

Model.knex(knex);

class m_agama extends Model {
    
    static get tableName() {
        return 'tbmst_agama';
    }

    static get idColumn() {
        return ['agm_nama'];
    }

    $beforeInsert() {
        this.created_at = formater.dateTimeGenerator();
    }
}

module.exports = m_agama;