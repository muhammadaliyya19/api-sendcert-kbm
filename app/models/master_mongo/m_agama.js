// const { Model } = require('objection');
// const knex = require('../../../config/database');
// const formater = require('../../../utils/formater');

// Model.knex(knex);

// class m_agama extends Model {
    
//     static get tableName() {
//         return 'tbmst_agama';
//     }

//     static get idColumn() {
//         return ['agm_nama'];
//     }

//     $beforeInsert() {
//         this.created_at = formater.dateTimeGenerator();
//     }
// }

// module.exports = m_agama;
const mongoose = require("mongoose");
mongoose.set('strictQuery', false);
const schema = mongoose.Schema;
const AgamaSchema = new schema({
    _id:{
        type:String,
        required:true
    },
    nama:{
        type:String,
        required:true
    }
});

module.exports = Agama = mongoose.model("agama",AgamaSchema);