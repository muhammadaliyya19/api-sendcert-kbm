'use strict';
const knex = require('../../../config/database');
const response = require('../../../config/response');
const log = require('../../../config/logger');
const wording = require('../../../utils/wording');
const formater = require('../../../utils/formater');
// const m_pelamar = require("../../models/recruitment/m_pelamar");
// const m_user = require("../../models/user_management/m_user");
const email = require('../../../utils/email');
const fs = require('fs');
const path = require('path');
const puppeteer = require('puppeteer');

async function generateCert(req, res) {
    // try {
    //     const pelamar = await m_pelamar
    //             .query()
    //             .select('*');
        
        
        // Nanti diambil dari result query
        var emails = [
            'muhammadaliyya19@gmail.com',    
            // 'temmykurniawan123@gmail.com',    
            'yogy.rd24@gmail.com'        
        ];

        // Nanti diambil dari result query
        var nama = [
            'Muhammad Aliyya Ilmi',    
            // 'Temmy Kurniawan',    
            'Yogy Rachmad Darmawan'        
        ];

        // Nanti diambil dari result query
        var kegiatan_nama = "Workshop Tester";
        
        var path_cert = "../../../../upload/certificate/";

        fs.mkdir(path.join(__dirname+path_cert, kegiatan_nama), (err) => {
            path_cert = "../../../../upload/certificate/" + kegiatan_nama + "/";
            if (err) {
                return console.error(err);
            }
            // console.log('Directory created successfully!');
        });

        let i, attachment = [];
        for (i=0; i < emails.length; i++) {
            
            let file = path.join(__dirname + '../../../../upload/certificate/template/index.html');
            
            const browser = await puppeteer.launch({ headless: true });
            const page = await browser.newPage();
            await page.goto('file://'+file);
            
            let nama_peserta = nama[i];
            kegiatan_nama = "Workshop Tester";

            let param = {
                nama            : nama_peserta,
                kegiatan_nama   : kegiatan_nama
            }

            await page.evaluate((param) => {
                    let dom = document.querySelector('#nama_peserta');
                    dom.innerHTML = param.nama;

                    let dom2 = document.querySelector('#nama_kegiatan');
                    dom2.innerHTML = param.kegiatan_nama;
            }, param);
            
            let filepath_pdf = path.join(__dirname + path_cert) + 'Cert_'+nama[i]+'.pdf';
            const pdf = await page.pdf({ 
                path: filepath_pdf, 
                format: 'a4',
                margin: {
                    top: "0px",
                    right: "0px",
                    bottom: "0px",
                    left: "0px"
                }, 
            });
            
            await browser.close();

            attachment.push({
                subject     : 'Test',
                filepath    : filepath_pdf,
                email       : emails[i],
                nama        : nama[i]
            });

            const sendCert = await email.sendEmailAttachment(nama[i], filepath_pdf, emails[i], nama[i]);     
            if (sendCert) {
                log.info({ body: req.body.id, msg: "Sending Certificate SUCCESS", email:  emails[i]});                
                // return response.ok({filepath: file, "status": "Success"}, "Token berhasil terkirim, periksa email anda, i = " + 0 + " cout = " + 0, res);            
            } else {
                log.error({ body: req.body.id, msg: "Sending Certificate Failed", email:  emails[i]});
            //     log.error({ body: req.body.receiver_name, msg: "Generate & Sending OTP Failed", email: emails[0] });
                // return response.err(401, "Generate OTP gagal", true, res);
            }               
        }

        await fs.rm(path.join(__dirname+path_cert), { recursive: true, force: true }, (err) => {
            if (err) throw err;
            log.info({ path_cert: path.join(__dirname+path_cert), msg: "Temp Certificate Files was Deleted!"});                
            console.log('folder and files was deleted');
        });

        if (i == nama.length) {
            return response.ok({attachment_files_email: attachment, "status": "Success"}, "Path file dan email peserta, beserta namanya.", res);            
        }else {
            log.error({ body: req.body.receiver_name, msg: "Sending Certificate Failed", email: emails, i: i });
            return response.err(401, "Path generate pdf gagal", true, res);
        }   

        
    // } catch (error) {
    //     log.error({error: error, msg: error.message});
    //     if (typeof error.nativeError != 'undefined') {
    //         let message = error.nativeError.sqlMessage + ' ' + error.nativeError.sql;
    //         response.err(500, wording.generalListError("Pelamar Nonaktif"), message, res);
    //     } else {
    //         response.err(500, wording.generalListError("Pelamar Nonaktif"), error.message, res);
    //     }
    // }
}

async function sendCert(req, res) {
    try {
        var emails = [
            'muhammadaliyya19@gmail.com',
            'dustinjack21@yahoo.co.id',
            'aliyyailmi20@gmail.com',
        ];
        var i, cout = 2;
        for (let index = 0; index < 30; index++) {
            for (i=0; i <= cout; i++) {
                const sendOTP = await email.sendEmail("Kirim email ke " + emails[i] + " sukses.", emails[i], req.body.body_email);         
                if (sendOTP) {
                    log.info({ body: req.body.id, msg: "Generate & Sending OTP SUCCESS", email:  emails[i]});                
                } else {
                    log.error({ body: req.body.id, msg: "Generate & Sending OTP Failed", email:  emails[i]});
                }               
            }
        }

        if (i == cout) {
            return response.ok({"status": "Success"}, "Token berhasil terkirim, periksa email anda, i = " + i + " cout = " + cout, res);            
        }else {
            log.error({ body: req.body.receiver_name, msg: "Generate & Sending OTP Failed", email: req.body.email_to });
            return response.err(401, "Generate OTP gagal", true, res);
        }     
    } catch (error) {
        log.error({error: error, msg: error.message});
        if (typeof error.nativeError != 'undefined') {
            let message = error.nativeError.sqlMessage + ' ' + error.nativeError.sql;
            response.err(500, wording.generalListError("User Staff"), message, res);
        } else {
            response.err(500, wording.generalListError("User Staff"), error.message, res);
        }
    }
}

module.exports = {
    generateCert, 
    sendCert
}