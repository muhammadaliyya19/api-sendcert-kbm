'use strict';
const response = require('../../../config/response');
const log = require('../../../config/logger');
const wording = require('../../../utils/wording');
const formater = require('../../../utils/formater');
const m_agama = require('../../models/master/m_agama');

async function getList(req, res) {
    try {
        // const agama = ['Islam', 'Hindu', 'Buddha', 'Kristen Katolik', 'Kristen Protestan', 'Konghucu'];
        const agama = await m_agama
            .query()
            .whereNull('deleted_at')
            .orderBy('agm_nama', 'asc');

        return response.ok(agama, wording.generalListSuccess("Agama"), res);
    } catch (error) {
        log.error({error: error, msg: error.message});
        if (typeof error.nativeError != 'undefined') {
            let message = error.nativeError.sqlMessage + ' ' + error.nativeError.sql;
            response.err(500, wording.generalListError("Agama"), message, res);
        } else {
            response.err(500, wording.generalListError("Agama"), error.message, res);
        }
    }
}

async function getByNama(req, res) {
    try {
        // const agama = null;
        const agama = await m_agama
            .query()
            .findById(req.query.agm_nama);

        return response.ok(agama, wording.generalDataSuccess("Agama"), res);
    } catch (e) {
        log.error({error: error, msg: error.message});
        if (typeof error.nativeError != 'undefined') {
            let message = error.nativeError.sqlMessage + ' ' + error.nativeError.sql;
            response.err(500, wording.generalDataError("Agama"), message, res);
        } else {
            response.err(500, wording.generalDataError("Agama"), error.message, res);
        }
    }
}

async function save(req, res) {
    try {
        req.body.values.created_by = req.userid;
        // const agama = null;
        const agama = await m_agama
            .query()
            .insert(req.body.values);

        return response.ok(agama, wording.generalSaveSuccess("Agama"), res);
    } catch (error) {
        log.error({error: error, msg: error.message});
        if (typeof error.nativeError != 'undefined') {
            let message = error.nativeError.sqlMessage + ' ' + error.nativeError.sql;
            response.err(500, wording.generalSaveError("Agama"), message, res);
        } else {
            response.err(500, wording.generalSaveError("Agama"), error.message, res);
        }
    }
}

async function update(req, res) {
    try {
        req.body.values.updated_by = req.userid;
        req.body.values.updated_at = formater.dateTimeGenerator();
        // const agama = null;
        const agama = await m_agama
            .query()
            .findById(req.body.conditions.agm_nama_old)
            .patch(req.body.values);

        if (agama > 0) {
            return response.ok(agama, wording.generalUpdateSuccess("Agama"), res);
        } else {
            return response.ok(agama, wording.generalUpdateSuccessNodata("Agama"), res);
        }
    } catch (e) {
        log.error({error: error, msg: error.message});
        if (typeof error.nativeError != 'undefined') {
            let message = error.nativeError.sqlMessage + ' ' + error.nativeError.sql;
            response.err(500, wording.generalUpdateError("Agama"), message, res);
        } else {
            response.err(500, wording.generalUpdateError("Agama"), error.message, res);
        }
    }
}

async function destroy(req, res) {
    try {
        // const agama = null;
        const agama = await m_agama
            .query()
            .findById(req.body.agm_nama)
            .patch({
                deleted_by: req.userid,
                deleted_at: formater.dateTimeGenerator()
            });

        if (agama > 0) {
            return response.ok(agama, wording.generalDeleteSuccess("Agama"), res);
        } else {
            return response.ok(agama, wording.generalDeleteSuccessNodata("Agama"), res);
        }
    } catch (e) {
        log.error({error: error, msg: error.message});
        if (typeof error.nativeError != 'undefined') {
            let message = error.nativeError.sqlMessage + ' ' + error.nativeError.sql;
            response.err(500, wording.generalDeleteError("Agama"), message, res);
        } else {
            response.err(500, wording.generalDeleteError("Agama"), error.message, res);
        }
    }
}

module.exports = {
    getList, getByNama, save, update, destroy
}
