const router = require("express").Router();

const c_agama = require('../controllers/master/c_agama');

//Master Agama
router.get('/master/agama/list', c_agama.getList);
router.get('/master/agama/by-nama', c_agama.getByNama);
router.post('/master/agama/save', c_agama.save);
router.put('/master/agama/update', c_agama.update);
router.delete('/master/agama/destroy', c_agama.destroy);

module.exports = router;