const generate_cert = require('./r_generate_cert');
const master = require('./r_master');
const master_mongo = require('./r_master_mongo');

module.exports = { 
    generate_cert,
    master,
    master_mongo
}