const router = require("express").Router();

const c_generate_cert = require('../controllers/cert/c_generate_cert');

router.get('/cert/generate-cert', c_generate_cert.generateCert);
router.get('/cert/send-cert', c_generate_cert.sendCert);

module.exports = router;