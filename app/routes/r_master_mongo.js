const router = require("express").Router();

const c_agama_mongo = require('../controllers/master/c_agama_mongo');

//Master Agama
router.get('/master_mongo/agama/list', c_agama_mongo.getList);
router.get('/master_mongo/agama/by-nama', c_agama_mongo.getByNama);
router.post('/master_mongo/agama/save', c_agama_mongo.save);
router.put('/master_mongo/agama/update', c_agama_mongo.update);
router.delete('/master_mongo/agama/destroy', c_agama_mongo.destroy);

module.exports = router;